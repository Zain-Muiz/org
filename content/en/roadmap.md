---
title: 'Roadmap 📅'
description: Grey Software's 2021 Roadmap
category: Info
position: 3
---

_This document is still a work in progress_

At the start of 2021, Grey software's team began to work on creating a brand new vision and strategy for the coming year. We called this effort V3. You can read more about this mile stone. 


The primary strategy for 2021 will be developing our open source projects and hosting education programs that empower people to create and use open-source software. 

Our first year is about building credibility, forming partnerships, and executing our programs. We will create high-quality open-source software and encourage other people to do so.

## Developing our Digital Brand

With the help of [Raj](https://www.linkedin.com/in/raj-paul-368827136/), our newly onboarded design lead, we're entirely redesigning our Grey Software website [on Figma](https://www.figma.com/file/3Deg3XBi1kTp79hcvg315e/Grey-Software-Website-Arsala?node-id=338%3A323)

## Running Educational Programs

We're running collaboration programs with universities, through which students can gain practical software development experience by participating in an agile, open-source software development environment.

More details, especially those about our upcoming programs with UET Mardan and AJKU, will be posted soon. 


## Developing Open Source Software Ecosystem

Our three main user-facing products will be:

### Open Governance

[Github Repository](https://gitlab.com/grey-software/open-gov)

### Focused Browsing

[Github Repository](https://gitlab.com/grey-software/focused-browsing/)
[App](http://focused-browsing.grey.software/)

### Material Math

[Github Repository](https://gitlab.com/grey-software/material-math/)
[App](http://material-math.grey.software/)
