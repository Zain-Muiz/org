---
title: PAK 🇵🇰 UETM
description:
position: 14
category: University Apprentices
spr2021:
  mentors:
    - name: Arsala
      avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
      github: https://github.com/ArsalaBangash
      gitlab: https://gitlab.com/ArsalaBangash
      linkedin: https://linkedin.com/in/ArsalaBangash
      website: https://arsalabangash.github.io/new-personal-website

    - name: Adil Shehzad
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7507821/avatar.png
      github: adilshehzad786
      gitlab: adilshehzad
      linkedin: adilshehzad7

  apprentices:
    - name: Faraz Ahmad Khan
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7891204/avatar.png
      github: https://github.com/farazahmadkhan15
      gitlab: https://gitlab.com/farazahmadkhan15
      linkedin: https://www.linkedin.com/in/farazahmadkhan15/
      website: https://farazahmadkhan.me/

    - name: Muhammad ismail
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8343421/avatar.png?width=400
      github: https://github.com/Muhammad0302
      gitlab: https://gitlab.com/Ismail030294
      linkedin: https://www.linkedin.com/in/muhammad-ismail-72681b177/
      website: https://muhammadismail47.netlify.app/

    - name: Abdurrahman-Alizada
      avatar: https://avatars.githubusercontent.com/u/79741876?s=400&u=b2dd682d25c93016bf5e9411ef76093c180b689a&v=4
      github: https://github.com/abdurrrahman-alizada
      gitlab: https://gitlab.com/abdurrrahman-alizada
      linkedin: https://www.linkedin.com/in/abdur-rahman-316b55203/
      website: https://abdurrahman-alizada.netlify.app/

    - name: Zakir Ullah Bangash
      avatar: https://avatars.githubusercontent.com/u/79740254?s=460u=b7c2d910da903c65083755499fbca43b773c8473&v=4
      github: https://github.com/zakirBangashUETM
      gitlab: https://gitlab.com/ZakirBangash
      linkedin: https://linkedin.com/in/zakir-bangash-6b33a0199/
      website: https://zakirbangash.netlify.app/

    - name: Saad Waseem
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8337704/avatar.png
      github: https://github.com/saad-waseem-12
      gitlab: https://gitlab.com/saad-waseem-12
      linkedin: https://www.linkedin.com/in/firebelias12
      website: https://saad-waseem.netlify.app/

    - name: Saifullah Saif
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8335114/avatar.png
      github: https://github.com/saifullah111
      gitlab: https://gitlab.com/saifullah111
      linkedin: https://www.linkedin.com/in/saifullah111/

    - name: Wasim Khan
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8335398/avatar.png
      github: https://github.com/Artaghal
      gitlab: https://gitlab.com/Artaghal
      linkedin: https://www.linkedin.com/in/wasim-cybernoob/
      website: https://artaghal.me
      
    - name: Omar-Bangash
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8344999/avatar.png?width=400
      github: https://github.com/Omar-Bangash
      gitlab: https://gitlab.com/Omar-Bangash
      linkedin: https://www.linkedin.com/in/omar-bangash-7b3139192/

    - name: Syed Asmar Haider
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8345338/avatar.png?width=400
      github: https://github.com/AsmarHiader
      gitlab: https://gitlab.com/AsmarHiader
      linkedin: https://www.linkedin.com/in/syed-asmar-haider-944b68184/

    - name: Muhammad Aqib
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8340234/avatar.png?width=400
      github: https://github.com/EngrAqib339
      gitlab: https://gitlab.com/EngrAqib339r
      linkedin: https://www.linkedin.com/in/muhammad-aqib-922456188

    - name: Laiba Gul
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8347443/avatar.png?width=400
      github: https://github.com/Laiba407
      gitlab: https://gitlab.com/Laiba407
      linkedin: https://www.linkedin.com/in/laiba-gul-797853206/
      website: https://laibagul.netlify.app/

    - name: Usman Nawaz
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8348088/avatar.png?width=400
      github: https://github.com/Usman-Nawaz001
      gitlab: https://gitlab.com/UsmanNawaz
      linkedin: https://www.linkedin.com/in/usman-nawaz-21532418a/

    - name: Zubair Ahmad Khan
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8349556/avatar.png?width=400
      github: https://github.com/zubairwazir
      gitlab: https://gitlab.com/zubairwazir
      linkedin: https://www.linkedin.com/in/zubair-ahmad-khan-836325171/

    - name: Kamran Bangash
      avatar: https://gitlab.com/uploads/-/system/user/avatar/8389739/avatar.png?width=400
      github: https://github.com/KamranKhanbangash
      gitlab: https://gitlab.com/KamranKhanbangash
      linkedin: https://www.linkedin.com/in/kamran-khan-33931ab6/
---

## University of Engineering & Technology Mardan

### Spring 2021 Cohort

### Mentors

<team-profiles :profiles="spr2021.mentors"></team-profiles>

### Apprentices

<team-profiles :profiles="spr2021.apprentices"></team-profiles>
