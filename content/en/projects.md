---
title: Current Projects
description: Grey Software's Current Projects
category: Info
position: 8
---
_This page is a work in progress_

## Org Ecosystem

### Grey Docs 

We are building a documentation website generator package to allow members of our organization to effectively create useful and beautiful websites for organizing their information. 

The package is built upon the Nuxt content docs theme, with added components and functionality suited for our organization's use cases. 

### Grey Blog

We are building a blog website generator package to allow members of our organization to effectively create useful and beautiful websites for sharing blog content. 

The package is built upon the Nuxt blog package, with added components and functionality suited for our organization's use cases. 

### Glossary


### Learn

### Blog

### Onboarding

### Investigating Vue state machines 
Reasoning about your UI as made up of machines that define states and transitions between those states allows you to tame the asynchronous complexity of app UI development. 

We at Grey Software want to investigate how we can get the latest technologies in application UI development using state machines to effectively work together. 

The technologies we are exploring are the Vue 3 composition API, VueX, and Xstate.



### Create a shop for Grey Software

The organization is currently raising money through GitHub Sponsors and OpenCollective. We hope to add a store for an additional source of income. 


### Education research from current UET Mardan program for other universities 

We will maintain a repository of our research from running applied open source software development programmes. 

Grey Software was founded on a commitment to finding the best ways for people to learn software development and contribute to spend source projects. 

## Nest software projects

Toonin
Open Mind 


## Active software projects

Open governance
Material Math
Focused Browsing

