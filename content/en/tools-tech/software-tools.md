---
title: Software Tools
description: An overview of the Software Tools we use at Grey Software
category: Tools and Tech
position: 17
---

## Github

![Github Preview](/tech-stack/github-preview.png)

GitHub is home to the largest software development community on the planet with over 50M+ developers and 100M+ projects. Grey software uses GitHub to manage collaborative open source software development projects and find the organization through the Github Sponsors program.

<cta-button  link="https://github.com/join" text="Sign Up" > </cta-button>

## Gitlab

![ Preview](/tech-stack/gitlab-preview.png)

Organizations rely on GitLab’s source code management, CI/CD, security, and more to deliver software rapidly. We use Gitlab along with Github because both services offer unique features that our organization takes advantage of.

<cta-button  link="https://gitlab.com/users/sign_up" text="Sign Up" > </cta-button>

<alert>
Why do we use both Github and Gitlab?
We use Gitlab for our primary development operations because we found it more conducive to handling the complexity of software project management. We use GitHub because of its vast community, classroom platform, and sponsorships program.</alert>

## VisBug

![VisBug Preview](/tech-stack/visbug-preview.png)

VisBug is an open-source web extension that allows users to interact with their webpage using powerful tools that can edit elements and inspect styles.

Our team at Grey Software highly recommends having this extension since it has allowed us to treat the webpage as an art-board that we can edit and inspect before heading back into the code editor.

<cta-button  link="https://addons.mozilla.org/en-US/firefox/addon/visbug/" text="Firefox Add-On" > </cta-button> <cta-button  link="https://chrome.google.com/webstore/detail/visbug/cdockenadnadldjbbgcallicgledbeoc?hl=en" text="Chrome Extension" > </cta-button>
